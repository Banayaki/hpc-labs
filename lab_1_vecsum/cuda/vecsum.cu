#include <cublas_v2.h>
#include <iostream>
#include <cmath>
#include <numeric>
#include <vector>
#include "kernel.cu"

using namespace std;

const int GPU_ID = 1;
const int NUM_STREAMS = 16;
const int THREDS_PER_BLOCK = 1024;

float *d_in[NUM_STREAMS] = {NULL}, *d_out[NUM_STREAMS] = {NULL}, *d_temp = {NULL};
int CACHED_N = -1;

cublasHandle_t HANDLE = NULL;
cudaStream_t streams[NUM_STREAMS] = {NULL};


#define CHECK_ERRORS_DECORATOR(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}


float* allocate_mem_device(const int n) {
    size_t vec_size = n * sizeof(float);
    float *dev_vec;
    CHECK_ERRORS_DECORATOR(cudaMalloc(&dev_vec, vec_size));
    return dev_vec;
}


float cpu_vecsum(const float *array, const int n) {
    std::vector<float> vec(array, array + n);

    // A little bit similar to my CUDA implementation
    // Must be better than default foreach loop
    float sum = std::accumulate(vec.begin(), vec.end(), 0.0);
    return sum;
}

float gpu_vecsum(const float *vec, const int n) {
    float sum = 0;
    // Temporary array for saving results from each stream
    float stream_results[NUM_STREAMS];

    // In each stream we need half elements of the part
    int threads_per_stream = n / 2 / NUM_STREAMS;
    int num_blocks_per_stream = threads_per_stream / THREDS_PER_BLOCK + (threads_per_stream % THREDS_PER_BLOCK > 0 ? 1 : 0);

    // Check if memory on GPU is already allocated
    // It would be better if I store initial vector in "Pinned method"
    // But I don't know how to implement it in a bueaty way, so...
    if (n != CACHED_N) {
        if (d_out[0] != NULL || d_in[0] != NULL) {
            for (int i = 0; i < NUM_STREAMS; i++) {
                CHECK_ERRORS_DECORATOR(cudaFree(d_out[i]));
                CHECK_ERRORS_DECORATOR(cudaFree(d_in[i]));
            }
        }
        CACHED_N = n;
        for (int i = 0; i < NUM_STREAMS; i++) {
            d_in[i] = allocate_mem_device(n / NUM_STREAMS);
            d_out[i] = allocate_mem_device(num_blocks_per_stream);
        }
    }

    if (streams == NULL) {
        for (int i = 0; i < NUM_STREAMS; i++) {
            CHECK_ERRORS_DECORATOR(
                cudaStreamCreate(&(streams[i]))
            );
        }
    }

    // Move data to device asynchronically
    for (int i = 0; i < NUM_STREAMS; i++) {
        cudaMemcpyAsync(d_in[i], vec + i * n / NUM_STREAMS, n / NUM_STREAMS * sizeof(float), cudaMemcpyHostToDevice, streams[i]);
    }

    int remaining_elements = n / NUM_STREAMS;
    while (remaining_elements > 1) {
        // Launch several kernels each in they own stream, it gains significant perfomance boost!
        for (int i = 0; i < NUM_STREAMS; i++) {
            reduce_sum<<<num_blocks_per_stream, THREDS_PER_BLOCK, 0, streams[i]>>>(d_in[i], d_out[i], remaining_elements);
        }

        remaining_elements = num_blocks_per_stream;
        threads_per_stream = remaining_elements / 2;
        num_blocks_per_stream = threads_per_stream / THREDS_PER_BLOCK + (threads_per_stream % THREDS_PER_BLOCK > 0 ? 1 : 0);

        if (remaining_elements > 1) {
            for (int i = 0; i < NUM_STREAMS; i++) {
                d_temp = d_in[i];
                d_in[i] = d_out[i];
                d_out[i] = d_temp;
            }
        }
    }

    for (int i = 0; i < NUM_STREAMS; i++) {
        cudaMemcpyAsync(&(stream_results[i]), d_out[i], sizeof(float), cudaMemcpyDeviceToHost, streams[i]);
    }

    CHECK_ERRORS_DECORATOR(cudaDeviceSynchronize());
    for (int i = 0; i < NUM_STREAMS; i++) {
        sum += stream_results[i];
    }
    return sum;
}