#include <cuda.h>
#include <iostream>
#include <device_functions.h>

__global__ void reduce_sum(float *vec, float *result, const int n) {
    // This is literally Reduce Sum implementation implemented in respect to CUDA concurecny
    // Shared memory is using to increase memory transfer perfomance 
    __shared__ float sdata[1024];

    // Check are we in the last block or not
    int chunk_size = (blockIdx.x * blockDim.x * 2 + blockDim.x * 2 > n) ? n % (blockDim.x * 2) : blockDim.x * 2;
    int tid = threadIdx.x;
    int block_start = blockIdx.x * (blockDim.x * 2) + threadIdx.x;

    // First reduction
    if (tid < chunk_size / 2) {
        sdata[tid] = vec[block_start] + vec[block_start + chunk_size / 2];
    }
    __syncthreads();

    for (int s = chunk_size / 4; s > 0; s >>= 1) {
        if (tid < s) {
            sdata[tid] += sdata[tid + s];
        }
        __syncthreads();
    }

    if (tid == 0) result[blockIdx.x] = sdata[0];

    // This is not ideal implementation, there is some things I can do. For example: 
    // As reduction proceeds than number of active threads decreases, so, when stride < 32
    // We do not need to syncronise threads (due to warp size)
}