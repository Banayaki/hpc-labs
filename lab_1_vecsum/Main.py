from functools import partial
from timeit import timeit
from typing import List

import numpy as np
import pandas as pd
import torch
from icecream import ic
from tqdm import tqdm

from CythonWrapper import c_cuda_vecsum, c_cpu_vecsum


def pytorch_cuda_vecsum(vec: np.ndarray, device: str = 'cuda:0') -> float:
    a = torch.from_numpy(vec).to(device)
    return torch.sum(a).detach().cpu().float()


def run_benchmark():
    """
        Launch a becnhamrk which measures average executing time
        for all implpemented methods
    """
    array_size = [2 ** i for i in range(7, 26)]
    repeats = 100
    results = {
        "c_cuda_vecsum": [],
        "pytorch_cuda_vecsum": [],
        "c_cpu_vecsum": [],
        "numpy_vecsum": [],
    }

    for N in tqdm(array_size[::]):
        test_vec = np.random.randn(N).astype(dtype=np.float32)

        results['pytorch_cuda_vecsum'] \
            .append(timeit(partial(pytorch_cuda_vecsum, test_vec), number=repeats) / repeats)
        results['c_cuda_vecsum'] \
            .append(timeit(partial(c_cuda_vecsum, test_vec), number=repeats) / repeats)
        results['c_cpu_vecsum'] \
            .append(timeit(partial(c_cpu_vecsum, test_vec), number=repeats) / repeats)
        results['numpy_vecsum'] \
            .append(timeit(partial(np.sum, test_vec), number=repeats) / repeats)

    df = pd.DataFrame(results)
    df.to_csv("torch.csv")


def perform_test(method, a, GT_result, **kwargs):
    """
        Checks method results equals to ground-truth result,
        otherwise raise AssertionError
    """
    res = method(a, **kwargs)
    ic(res, GT_result, method.__name__)
    assert np.isclose(GT_result, res), method.__name__ 


def check_correctness():
    """
        Performs sainity-check of implemented methods in the way
        comparison implementation resuls with np.sum result
    """
    vec = np.random.randn(2 ** 8).astype(dtype=np.float32)

    GT_result = np.sum(vec)

    perform_test(c_cuda_vecsum, vec, GT_result)
    perform_test(pytorch_cuda_vecsum, vec, GT_result)
    perform_test(c_cpu_vecsum, vec, GT_result)


if __name__ == "__main__":
    check_correctness()
    run_benchmark()
