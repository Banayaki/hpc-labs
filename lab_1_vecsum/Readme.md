# Lab №1 - Vector Sum
## Mukhin Artem 6133-010402D


# Summary 
In this work I made a comparison of the following implementations:
* Cython CPU **(using std::accumulate)**
* Cython GPU **(CUDA Kernel)**
* Python CPU **Numpy**
* Python GPU **Pytorch**

The project was executed inside Docker Container

# Motivation
In this work I wanna implement **reduce sum** on GPU using CUDA. So it will improve
my CUDA knowledge, specifically:

* Shared Memory
* Streams

# System and Software Specifications
* __OS__:  Ubuntu 20.04.2 LTS focal x86_64 
* __Kernel__:  4.15.0-154-generic 
* __CPU__: Intel i9-9940X (28) @ 4.400GHz 
* __Memory__: 128Gb
* __GPUs__: 2x RTX 2080ti

----

* __Image__: [nvcr.io/nvidia/pytorch:21.05-py3 (click for more info)](https://docs.nvidia.com/deeplearning/frameworks/pytorch-release-notes/rel_21-05.html#rel_21-05)
* __PyTorch__: 1.9.0
* __Python__: 3.8.8
* __CUDA__: 11.3.0
* __cuBLAS__: 11.5.1

# Run

To launch the benchmark just use following command: `make all`

Project requirements are listed in the `requirements.txt`

# Results

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>VectorLength</th>
      <th>c_cuda_vecsum</th>
      <th>c_cpu_vecsum</th>
      <th>numpy_vecsum</th>
      <th>pytorch_cuda_vecsum</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>128</td>
      <td>0.187</td>
      <td>0.002</td>
      <td>0.005</td>
      <td>0.059</td>
    </tr>
    <tr>
      <td>256</td>
      <td>0.163</td>
      <td>0.003</td>
      <td>0.005</td>
      <td>0.047</td>
    </tr>
    <tr>
      <td>512</td>
      <td>0.161</td>
      <td>0.005</td>
      <td>0.005</td>
      <td>0.040</td>
    </tr>
    <tr>
      <td>1024</td>
      <td>0.170</td>
      <td>0.008</td>
      <td>0.007</td>
      <td>0.035</td>
    </tr>
    <tr>
      <td>2048</td>
      <td>0.181</td>
      <td>0.016</td>
      <td>0.005</td>
      <td>0.034</td>
    </tr>
    <tr>
      <td>4096</td>
      <td>0.174</td>
      <td>0.032</td>
      <td>0.006</td>
      <td>0.035</td>
    </tr>
    <tr>
      <td>8192</td>
      <td>0.179</td>
      <td>0.065</td>
      <td>0.007</td>
      <td>0.038</td>
    </tr>
    <tr>
      <td>16384</td>
      <td>0.191</td>
      <td>0.137</td>
      <td>0.014</td>
      <td>0.043</td>
    </tr>
    <tr>
      <td>32768</td>
      <td>0.209</td>
      <td>0.276</td>
      <td>0.011</td>
      <td>0.055</td>
    </tr>
    <tr>
      <td>65536</td>
      <td>0.219</td>
      <td>0.508</td>
      <td>0.018</td>
      <td>0.082</td>
    </tr>
    <tr>
      <td>131072</td>
      <td>0.236</td>
      <td>1.110</td>
      <td>0.031</td>
      <td>0.132</td>
    </tr>
    <tr>
      <td>262144</td>
      <td>0.279</td>
      <td>2.244</td>
      <td>0.074</td>
      <td>0.225</td>
    </tr>
    <tr>
      <td>524288</td>
      <td>0.338</td>
      <td>4.499</td>
      <td>0.149</td>
      <td>0.332</td>
    </tr>
    <tr>
      <td>1048576</td>
      <td>0.429</td>
      <td>9.137</td>
      <td>0.308</td>
      <td>0.515</td>
    </tr>
    <tr>
      <td>2097152</td>
      <td>0.672</td>
      <td>18.422</td>
      <td>0.484</td>
      <td>0.944</td>
    </tr>
    <tr>
      <td>4194304</td>
      <td>1.136</td>
      <td>36.423</td>
      <td>1.245</td>
      <td>1.918</td>
    </tr>
    <tr>
      <td>8388608</td>
      <td>2.289</td>
      <td>73.750</td>
      <td>2.661</td>
      <td>4.047</td>
    </tr>
    <tr>
      <td>16777216</td>
      <td>4.401</td>
      <td>178.906</td>
      <td>5.670</td>
      <td>7.759</td>
    </tr>
    <tr>
      <td>33554432</td>
      <td>8.588</td>
      <td>359.306</td>
      <td>11.498</td>
      <td>15.516</td>
    </tr>
  </tbody>
</table>

![All Implementations](./images/AllImplementations.svg)
![Fast Implementations](./images/FastImplementations.svg)
![Fast Implementations 20+](./images/FastImplementations20+.svg)
![Acceleration](./images/GPUAccelerationrelativetoC++CPU.svg)

# References
* [Optimizing Parallel Reduction in CUDA](https://developer.download.nvidia.com/assets/cuda/files/reduction.pdf)
* [CUDA C/C++ Streams and Concurrency](https://developer.download.nvidia.com/CUDA/training/StreamsAndConcurrencyWebinar.pdf)