cimport cython

cdef extern from "cuda/vecsum.h":
    float gpu_vecsum(const float *vec, const int n)
    float cpu_vecsum(const float *vec, const int n)


def c_cuda_vecsum(float[:] vec):
    cdef:
        int n = vec.shape[0]
    return gpu_vecsum(&vec[0], n)


def c_cpu_vecsum(float[:] vec):
    cdef:
        int n = vec.shape[0]
    return cpu_vecsum(&vec[0], n)

