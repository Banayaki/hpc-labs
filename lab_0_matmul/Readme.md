# Lab №0 - Matrix Multiplication
## Mukhin Artem 6133-010402D


# Summary 
In this work I made a comparison of the following matrix multiplication methods:
* Python Native Multiprocessing
* Python Numpy Dot
* Python Native
* Python Pytorch GPU
* Python PyCuda Kernel
* Python PyCuda High-Level cuBLAS
* Cython cuBLAS
* Cython CPU
* Cython CPU OpenMP

The project was executed inside Docker Container

# Motivation
I'm usually working on Python language, thus I'm interested in getting knowledge of using CUDA inside Python.


In this work I especially wanna test two utils that makes me able to use CUDA on Python:
1. PyCuda _(Easy way)_
2. Cython _(Hard way)_


Additionally, due to using Cython I'll test different CPU implementations of matrix multiplication.


# System and Software Specifications 
* __OS__:  Ubuntu 20.04.2 LTS focal x86_64 
* __Kernel__:  4.15.0-154-generic 
* __CPU__: Intel i9-9940X (28) @ 4.400GHz 
* __Memory__: 128Gb
* __GPUs__: 2x RTX 2080ti

----

* __Image__: [nvcr.io/nvidia/pytorch:21.05-py3 (click for more info)](https://docs.nvidia.com/deeplearning/frameworks/pytorch-release-notes/rel_21-05.html#rel_21-05)
* __PyTorch__: 1.9.0
* __Python__: 3.8.8
* __CUDA__: 11.3.0
* __cuBLAS__: 11.5.1

# Run

To launch the benchmark just use following command: `make all`

Project requirements are listed in the `requirements.txt`

# Results

<table class="table table-bordered table-hover table-condensed">
<thead><tr><th title="Field #1">shape</th>
<th title="Field #2">vector_numpy_matmul</th>
<th title="Field #3">gpu_pytorch_matmul</th>
<th title="Field #4">gpu_pycuda_matmul</th>
<th title="Field #5">gpu_pycuda_cublas</th>
<th title="Field #6">cuda_matmul</th>
<th title="Field #7">parallel_matmul</th>
<th title="Field #8">single_thread_matmul</th>
<th title="Field #9">cpu_matmul</th>
<th title="Field #10">cpu_parallel_matmul</th>
</tr></thead>
<tbody><tr>
<td align="right">8</td>
<td align="right">0.002</td>
<td align="right">0.08</td>
<td align="right">0.553</td>
<td align="right">0.176</td>
<td align="right">0.028</td>
<td align="right">287.571</td>
<td align="right">0.467</td>
<td align="right">0.006</td>
<td align="right">0.057</td>
</tr>
<tr>
<td align="right">16</td>
<td align="right">0.003</td>
<td align="right">0.093</td>
<td align="right">0.533</td>
<td align="right">0.191</td>
<td align="right">0.031</td>
<td align="right">293.341</td>
<td align="right">10.938</td>
<td align="right">0.013</td>
<td align="right">0.092</td>
</tr>
<tr>
<td align="right">32</td>
<td align="right">0.004</td>
<td align="right">0.078</td>
<td align="right">0.567</td>
<td align="right">0.181</td>
<td align="right">0.031</td>
<td align="right">305.375</td>
<td align="right">39.649</td>
<td align="right">0.081</td>
<td align="right">0.65</td>
</tr>
<tr>
<td align="right">64</td>
<td align="right">0.008</td>
<td align="right">0.084</td>
<td align="right">0.514</td>
<td align="right">0.194</td>
<td align="right">0.036</td>
<td align="right">363.034</td>
<td align="right">234.205</td>
<td align="right">0.279</td>
<td align="right">0.32</td>
</tr>
<tr>
<td align="right">128</td>
<td align="right">0.695</td>
<td align="right">0.22</td>
<td align="right">0.74</td>
<td align="right">0.27</td>
<td align="right">0.067</td>
<td align="right">771.68</td>
<td align="right">1894.012</td>
<td align="right">2.147</td>
<td align="right">1.779</td>
</tr>
<tr>
<td align="right">256</td>
<td align="right">0.684</td>
<td align="right">1.243</td>
<td align="right">0.675</td>
<td align="right">0.388</td>
<td align="right">0.18</td>
<td align="right">4220.92</td>
<td align="right">15249.347</td>
<td align="right">16.615</td>
<td align="right">12.528</td>
</tr>
<tr>
<td align="right">512</td>
<td align="right">2.771</td>
<td align="right">2.08</td>
<td align="right">1.241</td>
<td align="right">1.336</td>
<td align="right">0.61</td>
<td align="right">32190.276</td>
<td align="right">125193.565</td>
<td align="right">255.501</td>
<td align="right">67.765</td>
</tr>
<tr>
<td align="right">1024</td>
<td align="right">11.251</td>
<td align="right">3.837</td>
<td align="right">4.073</td>
<td align="right">3.076</td>
<td align="right">1.753</td>
<td align="right">0.0</td>
<td align="right">0.0</td>
<td align="right">0.0</td>
<td align="right">0.0</td>
</tr>
<tr>
<td align="right">2048</td>
<td align="right">44.487</td>
<td align="right">13.243</td>
<td align="right">20.349</td>
<td align="right">10.622</td>
<td align="right">6.819</td>
<td align="right">0.0</td>
<td align="right">0.0</td>
<td align="right">0.0</td>
<td align="right">0.0</td>
</tr>
</tbody></table>

![All Implementations](./images/AllImplementations.svg)
![Python CPU Implementations](./images/PythonCPUImplementations.svg)
![Python GPU Implementations](./images/PythonGPUImplementations.svg)
![Cython Implementations](./images/CythonImplementations.svg)
![GPU Implementations](./images/GPUImplementations.svg)
![CPU Implementations](./images/CPUImplementations.svg)
![CPU Implementations 2](./images/CPUImplementations2.svg)
![Numpy vs Cublas](./images/NumpyvsCublas.svg)
![Numpy vs Cublas in times](./images/GPUAcceleration.svg)

# Conclusion
* __Cython:__

  \+ C++ in Python 

  \+ CUDA in Python

  \+ Fast as native C++

  \- Boilerplate code

  \- Need to bear in mind: C++ syntax, Code Compilation, Creating Libraries.

* __PyCuda:__

  \+ Ease of use

  \+ No need to compile 

  \+ Syntax Sugar and High-Level libraries (like scikit-cuda)

  \- Slower than Cython

  \- Poor error logging 

