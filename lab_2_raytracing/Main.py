from multiprocessing import shared_memory

import cv2
import pandas as pd
import numpy as np
from tqdm import tqdm

from lab_2_raytracing.engine.Camera import Camera
from lab_2_raytracing.engine.HittableList import HittableList
from lab_2_raytracing.engine.materials.Dielectric import Dielectric
from lab_2_raytracing.engine.materials.Lambertian import Lambertian
from lab_2_raytracing.engine.materials.Metal import Metal
from lab_2_raytracing.engine.Render import test_multiprocessing_render
from lab_2_raytracing.engine.Sphere import Sphere

from .CythonWrapper import cuda_render_small_world, cuda_render_big_world, clear_gpu_memory


CORES_COUNT = 8


def start_cuda_render():
    image = np.zeros((600, 1200, 3), dtype=np.int32)
    image = np.ascontiguousarray(image)

    print("Average frametime: ", np.mean([cuda_render_small_world(image, 1200, 600, 1000, 90.0) for _ in range(100)]))
    clear_gpu_memory()
    cv2.imwrite('cuda_test.png', image[::-1, ::, ::-1])


def benchmark():
    results_dict = {}
    gpu_repeats = 100
    cpu_repeats = 10

    resolutions = [
        (352, 240),
        (480, 360),
        (858, 480),
        (1280, 720),
    ]

    # World params
    world = HittableList()
    center_sphere_material = Lambertian(np.array([0.1, 0.2, 0.5]))
    left_sphere_material = Dielectric(1.5)
    right_sphere_material = Metal(np.array([0.8, 0.6, 0.2]), 0.25)
    surface_sphere_material = Lambertian(np.array([0.8, 0.8, 0.0]))

    world.add(Sphere(center=np.array(
        [0, 0, -1], dtype=np.float64), radius=0.5, material=center_sphere_material))
    world.add(Sphere(center=np.array(
        [-1, 0, -1], dtype=np.float64), radius=0.5, material=left_sphere_material))
    world.add(Sphere(center=np.array(
        [-1, 0, -1], dtype=np.float64), radius=-0.45, material=left_sphere_material))
    world.add(Sphere(center=np.array(
        [1, 0, -1], dtype=np.float64), radius=0.5, material=right_sphere_material))
    world.add(Sphere(center=np.array(
        [0, -100.5, -1], dtype=np.float64), radius=100, material=surface_sphere_material))

    samples_per_pixel = 10
    max_depth = 50

    for (width, height) in resolutions:
        key = f'{width}x{height}'
        results_dict[key] = {'cpu': [], 'gpu': []}
        fov = 30

        result = np.zeros((height, width, 3), dtype=np.float32)
        shm_image = shared_memory.SharedMemory(create=True, size=result.nbytes)
        image = np.ndarray(result.shape, dtype=np.float32,
                           buffer=shm_image.buf)
        cam = Camera(np.array([-2, 2, 1], dtype=np.float32), np.array([0, 0, -1],
                                                                      dtype=np.float32), np.array([0, 1, 0], dtype=np.float32), fov, width / height)

        results_cpu = []
        for i in tqdm(range(cpu_repeats),  desc=f'{key} CPU'):
            _time = test_multiprocessing_render(
                height, width, cam, world, max_depth, samples_per_pixel, CORES_COUNT, shm_image, result.shape)
            results_cpu.append(_time)

        cv2.imwrite(f'lab_2_raytracing/images/{key}_cpu.png', image[::-1, ::, ::-1])
        results_dict[key]['cpu'] = results_cpu

        image = np.zeros((height, width, 3), dtype=np.int32)
        image = np.ascontiguousarray(image)

        results_gpu = []
        for i in tqdm(range(gpu_repeats), desc=f'{key} GPU'):
            time = cuda_render_small_world(
                image, width, height, samples_per_pixel, fov)
            results_gpu.append(time)

        cv2.imwrite(
            f'lab_2_raytracing/images/{key}_gpu.png', image[::-1, ::, ::-1])
        results_dict[key]['gpu'] = results_gpu

        shm_image.close()
        shm_image.unlink()
    clear_gpu_memory()

    df = pd.DataFrame(results_dict)
    df.to_csv("results.csv")


def hard_benchmark():
    results_dict = {}
    gpu_repeats = 100

    resolutions = [
        (352, 240),
        (480, 360),
        (858, 480),
        (1280, 720),
    ]

    samples_per_pixel = 10
    max_depth = 50

    for (width, height) in resolutions:
        key = f'{width}x{height}'
        results_dict[key] = {'cpu': [], 'gpu': []}
        fov = 30

        image = np.zeros((height, width, 3), dtype=np.int32)
        image = np.ascontiguousarray(image)

        results_gpu = []
        for i in tqdm(range(gpu_repeats), desc=f'{key} GPU'):
            time = cuda_render_big_world(
                image, width, height, samples_per_pixel, fov)
            results_gpu.append(time)

        cv2.imwrite(
            f'lab_2_raytracing/images/hard_{key}_gpu.png', image[::-1, ::, ::-1])
        results_dict[key]['gpu'] = results_gpu

    clear_gpu_memory()

    df = pd.DataFrame(results_dict)
    df.to_csv("hard_results.csv")




if __name__ == "__main__":
    # from time import time
    # s = time()
    # multiprocessing_render()
    # ic(time() - s)
    start_cuda_render()
    # benchmark()
    # hard_benchmark()
