import numpy as np

from lab_2_raytracing.engine.utils.linalg import norm_vec

from .Ray import Ray


class Camera:
    def __init__(self, lookfrom: np.ndarray, lookat: np.ndarray, vup: np.ndarray, vertical_fov: float, aspect_ratio: float) -> None:
        self.aspect_ratio = aspect_ratio
        self.theta = np.deg2rad(vertical_fov)
        self.h = np.tan(self.theta / 2)
        self.viewport_height = 2.0 * self.h
        self.viewport_width = self.aspect_ratio * self.viewport_height

        w = norm_vec(lookfrom - lookat)
        u = norm_vec(np.cross(vup, w))
        v = np.cross(w, u)

        self.origin = lookfrom
        self.horizontal = self.viewport_width * u
        self.vertical = self.viewport_height * v

        self.bottom_left_corner = self.origin - self.horizontal / 2 - self.vertical / 2 - w

    def get_ray(self, u: float, v: float) -> Ray:
        return Ray(self.origin, self.bottom_left_corner + u * self.horizontal + v * self.vertical - self.origin)
