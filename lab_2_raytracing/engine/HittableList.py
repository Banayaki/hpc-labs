from typing import List, Optional

from .Ray import Ray
from .Hittable import Hittable, HitRecord


class HittableList(Hittable):
    def __init__(self, hittable_objects: List[Hittable] = None):
        if hittable_objects is None:
            self.hittable_objects = []
        else:
            self.hittable_objects = hittable_objects

    def hit(self, r: Ray, t_min: float, t_max: float) -> Optional[HitRecord]:
        record = None
        closest_so_far = t_max

        for obj in self.hittable_objects:
            hit_record = obj.hit(r, t_min, closest_so_far)
            if hit_record is not None:
                record = hit_record
                closest_so_far = record.t
        
        return record

    def clear(self):
        self.hittable_objects = []
    
    def add(self, hittable: Hittable):
        self.hittable_objects.append(hittable)