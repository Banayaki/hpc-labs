from typing import Optional, Tuple
from lab_2_raytracing.engine.HitRecord import HitRecord
from lab_2_raytracing.engine.Ray import Ray
from lab_2_raytracing.engine.utils.linalg import norm_vec
from lab_2_raytracing.engine.utils.random import random_in_unit_sphere, random_unit_vector
from numba import jit
import numpy as np


@jit(nopython=True)
def reflect(v, n):
    return v - 2 * np.dot(v, n) * n


@jit(nopython=True)
def refract(uv: np.ndarray, n: np.ndarray, refraction_ratio: float, cos_theta: float):
    r_out_perp = refraction_ratio * (uv + cos_theta * n)
    r_out_parallel = - np.sqrt(np.abs(1.0 - np.sum(r_out_perp ** 2))) * n
    return r_out_perp + r_out_parallel


@jit(nopython=True)
def reflectance(cosine: float, ref_idx: float):
    r0 = (1.0 - ref_idx) / (1.0 + ref_idx)
    r0 = r0 * r0
    return r0 + (1 - r0) * np.power(1 - cosine, 5)


@jit(nopython=True)
def calc_trigonometry(unit_direction, normal):
    cos_theta = min(np.dot(-unit_direction, normal), 1.0)
    sin_theta = np.sqrt(1.0 - cos_theta * cos_theta)
    return cos_theta, sin_theta


class Reflector:

    def scatter(self, ray_in: Ray, hit_record: HitRecord) -> Optional[Tuple[HitRecord, Ray, np.ndarray]]:
        """
        Returns: isOutside, hitRecord, bouncedRay and attenuation
        """
        material = hit_record.material
        return getattr(self, 'scatter_' + material.get_name())(ray_in, hit_record)

    def scatter_metal(self, ray_in: Ray, hit_record: HitRecord) -> Optional[Tuple[bool, HitRecord, Ray, np.ndarray]]:
        reflected = reflect(norm_vec(ray_in.direction()), hit_record.normal)
        scattered = Ray(hit_record.p, reflected + hit_record.material.fuzz * random_in_unit_sphere())
        attenuation = hit_record.material.albedo
        return np.dot(scattered.direction(), hit_record.normal) > 0, hit_record, scattered, attenuation


    def scatter_lambertian(self, ray_in: Ray, hit_record: HitRecord) -> Optional[Tuple[bool, HitRecord, Ray, np.ndarray]]:
        scatter_direction = hit_record.normal + random_unit_vector()

        if np.allclose(scatter_direction, 0.0):
            scatter_direction = hit_record.normal

        scattered = Ray(hit_record.p, scatter_direction)
        attenuation = hit_record.material.albedo
        return True, hit_record, scattered, attenuation

    def scatter_dielectric(self, ray_in: Ray, hit_record: HitRecord):
        ir = hit_record.material.ir
        attenuation = np.array([1.0, 1.0, 1.0])
        refraction_ratio = 1.0 / ir if hit_record.front_face else ir

        unit_direction = norm_vec(ray_in.direction())

        cos_theta, sin_theta = calc_trigonometry(unit_direction, hit_record.normal)

        is_cannot_refract = refraction_ratio * sin_theta > 1.0

        if is_cannot_refract or reflectance(cos_theta, refraction_ratio) > np.random.random():
            direction = reflect(unit_direction, hit_record.normal)
        else:
            direction = refract(unit_direction, hit_record.normal, refraction_ratio, cos_theta)
        
        scattered = Ray(hit_record.p, direction)
        return True, hit_record, scattered, attenuation