from typing import Optional, Tuple
import numpy as np
from lab_2_raytracing.engine.materials.Material import Material



class Lambertian(Material):
    def __init__(self, albedo: np.ndarray) -> None:
        super().__init__()
        self.albedo = albedo

    def get_name(self) -> str:
        return 'Lambertian'.lower()

