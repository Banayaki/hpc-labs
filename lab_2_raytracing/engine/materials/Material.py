from abc import ABC, abstractmethod
from typing import Optional
import numpy as np


class Material(ABC):
    @abstractmethod
    def get_name(self) -> str:
        pass
