from typing import Optional, Tuple
import numpy as np
from lab_2_raytracing.engine.materials.Material import Material


class Metal(Material):
    def __init__(self, albedo: np.ndarray, fuzz: float) -> None:
        super().__init__()
        self.albedo = albedo
        self.fuzz = fuzz

    def get_name(self) -> str:
        return 'Metal'.lower()

