from typing import Optional, Tuple
import numpy as np
from lab_2_raytracing.engine.materials.Material import Material



class Dielectric(Material):
    def __init__(self, index_of_refraction: float) -> None:
        super().__init__()
        self.ir = index_of_refraction

    def get_name(self) -> str:
        return 'Dielectric'.lower()

