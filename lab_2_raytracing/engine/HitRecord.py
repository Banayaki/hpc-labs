from typing import List
from dataclasses import dataclass
import numpy as np
from numba import jit
from lab_2_raytracing.engine.materials.Material import Material
from lab_2_raytracing.engine.Ray import Ray


@dataclass
class HitRecord:
    p: List[int]
    normal: np.ndarray
    material: Material
    t: float
    front_face: bool

    def set_face_normal(self, ray: Ray, outward_normal: np.ndarray):
        self.front_face = HitRecord.check_face_normal(ray.direction(), outward_normal)
        self.normal = outward_normal if self.front_face else -outward_normal

    @staticmethod
    @jit(nopython=True)
    def check_face_normal(direction, outward):
        return np.dot(direction, outward) < 0