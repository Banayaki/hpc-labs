import numpy as np
from numba import jit


class Ray:
    def __init__(self, a: np.ndarray, b: np.ndarray) -> None:
        self.a = a
        self.b = b
    
    def origin(self):
        return self.a

    def direction(self):
        return self.b

    def __getitem__(self, t: float):
        return Ray.calculate_ray(self.a, self.b, t)

    @staticmethod
    @jit(nopython=True)
    def calculate_ray(a, b, t):
        return a + t * b