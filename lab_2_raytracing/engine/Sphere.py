from typing import Optional

import numpy as np
from lab_2_raytracing.engine.materials.Material import Material
from numba import jit

from .Ray import Ray
from .Hittable import Hittable, HitRecord


class Sphere(Hittable):

    def __init__(self, center: np.ndarray, radius: float, material: Material) -> None:
        super().__init__()
        self.center = center
        self.R = radius
        self.material = material
        
    def hit(self, r: Ray, t_min: float, t_max: float) -> Optional[HitRecord]:
        root = Sphere.fast_compute(r.origin(), r.direction(), t_min, t_max, self.center, self.R)
        if root is None:
            return None

        point = r[root]
        record = HitRecord(t=root, p=point, normal=None, front_face=None, material=self.material)
        record.set_face_normal(r, Sphere.norm_radius_vec(point, self.center, self.R))

        return record

    @staticmethod
    @jit(nopython=True)
    def norm_radius_vec(point, center, r):
        return (point - center) / r

    @staticmethod
    @jit(nopython=True)
    def fast_compute(origin, direction, t_min, t_max, center, R):
        oc = origin - center
        A = np.dot(direction, direction)
        B = np.dot(oc, direction)
        C = np.dot(oc, oc) - R * R

        discriminant = B * B - A * C
        if discriminant < 0:
            return None

        root = (-B - discriminant ** 0.5) / A
        if root < t_min or root > t_max:
            root = (-B + discriminant ** 0.5) / A
            if root < t_min or root > t_max:
                return None
        
        return root