from numba import jit
import numpy as np


@jit(nopython=True)
def random_in_unit_sphere():
    while True:
        target = random_float(3) * 2 - 1
        if np.linalg.norm(target) < 1:
            return target


@jit(nopython=True)
def random_unit_vector():
    while True:
        target = random_float(3) * 2 - 1
        if np.linalg.norm(target) < 1:
            return target / np.linalg.norm(target)


@jit(nopython=True)
def random_in_hemisphere(normal):
    in_unit_sphere = random_in_unit_sphere()
    if (np.dot(in_unit_sphere, normal) > 0):
        return in_unit_sphere
    else:
        return -in_unit_sphere


@jit(nopython=True)
def random_float(size=1):
    return np.random.random(size)
