import numpy as np
from numba import jit


@jit(nopython=True)
def norm_vec(vec: np.ndarray) -> np.ndarray:
    return vec / np.linalg.norm(vec)