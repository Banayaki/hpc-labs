from functools import partial
from multiprocessing import Pool, shared_memory, Process
from typing import List, Tuple

import cv2
import numpy as np
from numba import jit
from tqdm import tqdm
from time import time

from lab_2_raytracing.engine.Buffer import Buffer
from lab_2_raytracing.engine.Camera import Camera
from lab_2_raytracing.engine.Hittable import Hittable
from lab_2_raytracing.engine.HittableList import HittableList
from lab_2_raytracing.engine.materials.Dielectric import Dielectric
from lab_2_raytracing.engine.materials.Lambertian import Lambertian
from lab_2_raytracing.engine.materials.Metal import Metal
from lab_2_raytracing.engine.materials.Reflector import Reflector
from lab_2_raytracing.engine.Ray import Ray
from lab_2_raytracing.engine.Sphere import Sphere
from lab_2_raytracing.engine.utils.linalg import norm_vec
from lab_2_raytracing.engine.utils.random import (random_float,
                                                  random_unit_vector)


@jit(nopython=True)
def get_UV(w, h, width, height):
    u = (w + random_float()) / (width - 1)
    v = (h + random_float()) / (height - 1)
    return u, v


@jit(nopython=True)
def mix_background_color(y, color_1, color_2):
    t = 0.5 * (y + 1.0)
    return (1.0 - t) * color_1 + t * color_2


def ray_color(r: Ray, world: Hittable, depth: int, color_1: np.ndarray = np.array([1.0, 1.0, 1.0]), color_2: List[int] = np.array([0.5, 0.7, 1.0])):
    if depth <= 0:
        return np.array([0, 0, 0])

    record = world.hit(r, 0.001, +np.Inf)
    if record is not None:
        is_outside, record, scattered, attenuation = Reflector().scatter(r, record)
        if is_outside:
            return attenuation * ray_color(scattered, world, depth - 1)
        return np.array([0, 0, 0])

    unit_direction = norm_vec(r.direction())
    return mix_background_color(unit_direction[1], color_1, color_2)


def get_color(color: np.ndarray, samples_per_pixel: int):
    scale = 1.0 / samples_per_pixel
    color *= scale
    color = np.power(color, 0.5)

    return 256 * np.clip(color, 0.0, 0.999)


def ray_generator(height, width, samples_per_pixel, cam):
    for h in range(height):
        for w in range(width):
            for i in range(samples_per_pixel):
                u, v = get_UV(w, h, width, height)
                r = cam.get_ray(u, v)
                yield (h, w, r)


def core_render_func(params, world, max_depth, shm_name, shm_shape):
    h, w, r = params
    shm = shared_memory.SharedMemory(name=shm_name)
    result = np.ndarray(shm_shape, dtype=np.float32, buffer=shm.buf)
    result[h, w] += ray_color(r, world, max_depth)
    del result
    shm.close()


def test_multiprocessing_render(height, width, camera, world, max_depth, number_samples, cores_count, shm, shape):
    parameters = [i for i in ray_generator(height, width, number_samples, camera)]
    result = np.zeros((height, width, 3), dtype=np.float32)

    s = time()
    image = np.ndarray(shape, dtype=np.float32, buffer=shm.buf)
    with Pool(cores_count) as pool:
        # Generator does not work how it SHOULD!
        pool.map(partial(core_render_func, world=world, max_depth=max_depth,
                 shm_name=shm.name, shm_shape=result.shape), parameters)

    scale = 1.0 / number_samples
    image *= scale
    np.power(image, 0.5, out=image)

    np.clip(image, 0.0, 0.999, out=image)
    image *= 256 

    finish = time() - s
    return finish


def test_render():
    aspect_ratio = 16.0 / 9.0
    width = 400
    height = int(width / aspect_ratio)
    buffer = Buffer(height, width)

    # World params
    world = HittableList()
    center_sphere_material = Lambertian(np.array([0.1, 0.2, 0.5]))
    left_sphere_material = Dielectric(1.5)
    right_sphere_material = Metal(np.array([0.8, 0.6, 0.2]), 0.25)
    surface_sphere_material = Lambertian(np.array([0.8, 0.8, 0.0]))

    world.add(Sphere(center=np.array(
        [0, 0, -1], dtype=np.float64), radius=0.5, material=center_sphere_material))
    world.add(Sphere(center=np.array(
        [-1, 0, -1], dtype=np.float64), radius=0.5, material=left_sphere_material))
    world.add(Sphere(center=np.array(
        [-1, 0, -1], dtype=np.float64), radius=-0.45, material=left_sphere_material))
    world.add(Sphere(center=np.array(
        [1, 0, -1], dtype=np.float64), radius=0.5, material=right_sphere_material))
    world.add(Sphere(center=np.array(
        [0, -100.5, -1], dtype=np.float64), radius=100, material=surface_sphere_material))

    samples_per_pixel = 10
    max_depth = 20

    cam = Camera(90, aspect_ratio)

    for h in tqdm(range(height)):
        for w in range(width):
            color = np.zeros((3,))
            for i in range(samples_per_pixel):
                u, v = get_UV(w, h, width, height)

                r = cam.get_ray(u, v)
                color += ray_color(r, world, max_depth)

            buffer[h, w] = get_color(color, samples_per_pixel)
    buffer.write_to_file('test.png')
