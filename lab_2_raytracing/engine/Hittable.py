from abc import ABC, abstractmethod
from typing import List, Optional

from dataclasses import dataclass

import numpy as np
from lab_2_raytracing.engine.HitRecord import HitRecord
from numba import jit

from .Ray import Ray


class Hittable(ABC):

    @abstractmethod
    def hit(self, r: Ray, t_min: float, t_max: float) -> Optional[HitRecord]:
        pass