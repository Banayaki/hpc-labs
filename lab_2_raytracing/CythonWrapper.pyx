cimport cython
cimport numpy as cnp

cdef extern from "cuda/main.h":
    float small_launch_render(int *image, int nx, int ny, int ns, float fov)
    float big_launch_render(int *image, int nx, int ny, int ns, float fov)
    void clear_cuda()


def cuda_render_small_world(cnp.ndarray[int, ndim=3] image, int nx, int ny, int ns, float fov):
    return small_launch_render(&image[0, 0, 0], nx, ny, ns, fov)

    
def cuda_render_big_world(cnp.ndarray[int, ndim=3] image, int nx, int ny, int ns, float fov):
    return big_launch_render(&image[0, 0, 0], nx, ny, ns, fov)


def clear_gpu_memory():
    clear_cuda()