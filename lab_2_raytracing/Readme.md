# Lab №2 - Raytracing
## Mukhin Artem 6133-010402D


# Summary 
In this work I impleneted raytracing using both GPU and CPU computation. 
* Cython GPU **(CUDA Kernel)**
* Python CPU **Multiprocessing**

The project was executed inside Docker Container

# Implementation details
## Python CPU
Python CPU raytracing is extremly slow, espicially without using **multiprocessing** and **Numba Just In Time** compilation.

I've used 8 processes for multiprocessing.

BTW my implementation has some space for improvement, but there is *no any sense* to do that. 

## CUDA
For future optimization OptiX should be used

## References
* [CPU Implementation](https://raytracing.github.io/books/RayTracingInOneWeekend.html)

* [GPU Implementation](https://developer.nvidia.com/blog/accelerated-ray-tracing-cuda/)

* [Advanced CUDA Implementation](https://joaovbs96.github.io/posts/next-week/)

# System and Software Specifications
* __OS__:  Ubuntu 20.04.2 LTS focal x86_64 
* __Kernel__:  4.15.0-154-generic 
* __CPU__: Intel i9-9940X (28) @ 4.400GHz 
* __Memory__: 128Gb
* __GPUs__: 2x RTX 2080ti

----

* __Image__: [nvcr.io/nvidia/pytorch:21.05-py3 (click for more info)](https://docs.nvidia.com/deeplearning/frameworks/pytorch-release-notes/rel_21-05.html#rel_21-05)
* __PyTorch__: 1.9.0
* __Python__: 3.8.8
* __CUDA__: 11.3.0
* __cuBLAS__: 11.5.1

# Run

To launch the benchmark just use following command: `make all`

Project requirements are listed in the `requirements.txt`

# Results

## Easy scene

![Example](./images/1280x720_gpu.png)

### Extremly slow CPU 

<table border="1" class="dataframe">
  <tbody>
    <tr>
      <td>
        <img src="./images/352x240_cpu.gif"/>
      </td>
      <td>
        <img src="https://preview.redd.it/d8qmyf3lcl861.jpg?auto=webp&s=086664a075fa6a0c4591f83b6b4038f4de95919b" width="352px"/>
      </td>
    </tr>
    <tr>
      <td>
        <img src="https://s3.wi-fi.ru/cp3o/VF2uMXX3pnu3XEmAZPpkYvUM?response-content-type=image%2Fpng" width="352px"/>
      </td>
      <td>
        <img src="https://ak.picdn.net/shutterstock/videos/1012976231/thumb/1.jpg?ip=x480" width="352px"/>
      </td>
    </tr>
  </tbody>
</table>

![CPU_VS_GPU](./images/cpu_vs_gpu.svg)
![Acceleration](./images/acceleration.svg)
![CPU Framerate](./images/cpu_frametime.svg)
![GPU Framerate](./images/gpu_frametime.svg)

### Summary table for GPU on easy scene

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>352x240</th>
      <th>480x360</th>
      <th>858x480</th>
      <th>1280x720</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>FPS</th>
      <td>110.894252</td>
      <td>67.675574</td>
      <td>34.189869</td>
      <td>16.887274</td>
    </tr>
    <tr>
      <th>1% LOW FPS</th>
      <td>94.605589</td>
      <td>59.310222</td>
      <td>32.241632</td>
      <td>16.188040</td>
    </tr>
    <tr>
      <th>0.1% LOW FPS</th>
      <td>88.440787</td>
      <td>58.592605</td>
      <td>30.873728</td>
      <td>14.214035</td>
    </tr>
    <tr>
      <th>AVG. Framerate</th>
      <td>0.009018</td>
      <td>0.014776</td>
      <td>0.029248</td>
      <td>0.059216</td>
    </tr>
  </tbody>
</table>

## Hard Scene

![Hard scene](./images/hard_1280x720_gpu.png)

![GPU](./images/hard_gpu.svg)
![GPU Framerate](./images/hard_gpu_frametime.svg)


<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>352x240</th>
      <th>480x360</th>
      <th>858x480</th>
      <th>1280x720</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>FPS</th>
      <td>1.490238</td>
      <td>0.781394</td>
      <td>0.377254</td>
      <td>0.168914</td>
    </tr>
    <tr>
      <th>1% LOW FPS</th>
      <td>1.403383</td>
      <td>0.739926</td>
      <td>0.366272</td>
      <td>0.165419</td>
    </tr>
    <tr>
      <th>0.1% LOW FPS</th>
      <td>1.382162</td>
      <td>0.720968</td>
      <td>0.359588</td>
      <td>0.163001</td>
    </tr>
    <tr>
      <th>AVG. Framerate</th>
      <td>0.671034</td>
      <td>1.279765</td>
      <td>2.650733</td>
      <td>5.920159</td>
    </tr>
  </tbody>
</table>

## Scene with light

This scene was rendered with 1000 samples per ray.

**Average frametime:  5.130s.**

![Scene with light](./images/cuda_sun.png)


