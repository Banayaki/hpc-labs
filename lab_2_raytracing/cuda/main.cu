#include <iostream>
#include <time.h>
#include <float.h>
#include <curand_kernel.h>

#include "include/vec3.h"
#include "include/ray.h"
#include "include/sphere.h"
#include "include/hitable_list.h"
#include "include/camera.h"
#include "include/material.h"

// Max recursion depth or in CUDA case number of iterations 
__constant__ int RAY_MAX_BOUNCE = 50;
__constant__ int SEED = 777;

// A lot of different parameters which using for caching between launches
int CACHED_NX = NULL, CACHED_NY = NULL, CACHED_FOV = NULL, CACHED_NUM_SPHERES = NULL;

int tx = 16;
int ty = 16;
dim3 blocks = NULL;
dim3 threads = NULL;

size_t fb_size = NULL;
vec3 *fb;

curandState *d_rand_state_world;
curandState *d_rand_state;


hitable **d_list;
hitable **d_world;
camera **d_camera;


// Just a shortcut 
#define RND (curand_uniform(&local_rand_state))


#define checkCudaErrors(val) check_cuda( (val), #val, __FILE__, __LINE__ )
void check_cuda(cudaError_t result, char const *const func, const char *const file, int const line) {
    /**
     * @brief limited version of checkCudaErrors from helper_cuda.h in CUDA examples
     */
    if (result) {
        std::cerr << "CUDA error = " << static_cast<unsigned int>(result) << " at " <<
            file << ":" << line << " '" << func << "' \n";
        // Make sure we call CUDA Device Reset before exiting
        cudaDeviceReset();
        exit(-1);
    }
}

__device__ vec3 color(const ray& r, hitable **world, curandState *local_rand_state) {
    /**
     * @brief CUDA version of ray bouncing. Contrastly to CPU version here we user loop despite of 
     * recursion. It gives us huge perfomance boost 
     */
    ray cur_ray = r;
    vec3 cur_attenuation = vec3(1.0, 1.0, 1.0);

    for (int i = 0; i < RAY_MAX_BOUNCE; i++) {
        hit_record rec;

        // 0.001 prevents reflecting near zero 
        if (!(*world)->hit(cur_ray, 0.001f, FLT_MAX, rec)) {
            return cur_attenuation * vec3(0.0, 0.0, 0.0);
        }

        ray scattered;
        vec3 attenuation;
        vec3 emmited = rec.mat_ptr->emitted(rec.u, rec.v, rec.p);
        
        if(rec.mat_ptr->scatter(cur_ray, rec, attenuation, scattered, local_rand_state)) {
            cur_attenuation = emmited + cur_attenuation * attenuation;
            cur_ray = scattered;
        } else { 
            return cur_attenuation * emmited;
        }
    }
    return vec3(0.0, 0.0, 0.0); 
}

__global__ void rand_init(curandState *rand_state) {
    /**
     * @brief initializes curand for world generator
     */
    if (threadIdx.x == 0 && blockIdx.x == 0) {
        curand_init(SEED, 0, 0, rand_state);
    }
}

__global__ void render_init(int max_x, int max_y, curandState *rand_state) {
    /**
     * @brief  initializes curand for each thread
     * @details
     * Each thread gets different seed but freezing SEED variables makes us able
     * to reproduce the image after restarting
     */
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    if ((i >= max_x) || (j >= max_y)) return;

    int pixel_index = j * max_x + i;
    curand_init(SEED + pixel_index, 0, 0, &rand_state[pixel_index]);
}

__global__ void render(vec3 *fb, int max_x, int max_y, int ns, camera **cam, hitable **world, curandState *rand_state) {
    /**
     * @brief launches a ray from each pixel and starts tracking him
     * @details
     * Also this method uses simple antialiasing by adding some noise in UV
     */
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    if((i >= max_x) || (j >= max_y)) return;

    int pixel_index = j * max_x + i;
    curandState local_rand_state = rand_state[pixel_index];
    vec3 col(0, 0, 0);

    for(int s = 0; s < ns; s++) {
        float u = float(i + RND) / float(max_x);
        float v = float(j + RND) / float(max_y);
        ray r = (*cam)->get_ray(u, v, &local_rand_state);
        col += color(r, world, &local_rand_state);
    }

    rand_state[pixel_index] = local_rand_state;
    col /= float(ns);
    col[0] = sqrt(col[0]);
    col[1] = sqrt(col[1]);
    col[2] = sqrt(col[2]);
    fb[pixel_index] = col;
}

__global__ void set_camera(camera **d_camera, vec3 lookfrom, vec3 lookat, float fov, int nx, int ny) {
    /**
     * @brief makes us able to control camera parameters
     */
    float dist_to_focus = (lookfrom - lookat).length();
    float aperture = 0.1;
    vec3 vup(0, 1, 0);
    float aspect_ratio = float(nx) / float(ny);
    *d_camera   = new camera(lookfrom,
                             lookat,
                             vup,
                             fov,
                             aspect_ratio,
                             aperture,
                             dist_to_focus);
}


__global__ void craete_small_world(hitable **d_list, hitable **d_world, curandState *rand_state) {
    if (threadIdx.x == 0 && blockIdx.x == 0) {
        curandState local_rand_state = *rand_state;

        // Surface
        d_list[0] = new sphere(vec3(0, -100.5, -1), 100,
                               new lambertian(vec3(0.8, 0.8, 0.0)));

        d_list[1] = new sphere(vec3(2, 2, -2), 0.5, new diffuse_light(vec3(4.0, 4.0, 4.0)));

        d_list[2] = new sphere(vec3(-1, 0, -1), 0.5, new metal(vec3(0.8, 0.6, 0.2), 0.25));
        d_list[3] = new sphere(vec3(0, 0, -1), 0.5, new metal(vec3(0.8, 0.6, 0.2), 0.25));
        d_list[4] = new sphere(vec3(1, 0, -1), 0.5, new metal(vec3(0.8, 0.6, 0.2), 0.25));

        *rand_state = local_rand_state;
        *d_world  = new hitable_list(d_list, 5);
    }
}

__global__ void create_world(hitable **d_list, hitable **d_world, curandState *rand_state, int num_hitables) {
    /**
     * @brief generates the world state
     * @details
     * Random generates 482 spheres randomly placed on a surface (the surface is a one big sphere)
     */
    if (threadIdx.x == 0 && blockIdx.x == 0) {
        curandState local_rand_state = *rand_state;

        // Surface
        d_list[0] = new sphere(vec3(0,-1000.0,-1), 1000,
                               new lambertian(vec3(0.5, 0.5, 0.5)));

        int i = 1;
        for(int a = -11; a < 11; a++) {
            for(int b = -11; b < 11; b++) {
                float choose_mat = RND;
                vec3 center(a + RND, 0.2, b + RND);
                if(choose_mat < 0.8f) {
                    d_list[i++] = new sphere(center, 0.2,
                                             new lambertian(vec3(RND * RND, RND * RND, RND * RND)));
                }
                else if(choose_mat < 0.95f) {
                    d_list[i++] = new sphere(center, 0.2,
                                             new metal(vec3(0.5f * (1.0f + RND), 0.5f * (1.0f + RND), 0.5f * (1.0f + RND)), 0.5f * RND));
                }
                else {
                    d_list[i++] = new sphere(center, 0.2, new dielectric(1.5));
                }
            }
        }
        d_list[i++] = new sphere(vec3(0, 1,0),  1.0, new dielectric(1.5));
        d_list[i++] = new sphere(vec3(4, 1, 0), 1.0, new lambertian(vec3(0.1, 0.2, 0.5)));
        d_list[i++] = new sphere(vec3(-4, 1, 0),  1.0, new metal(vec3(0.7, 0.6, 0.5), 0.0));
        *rand_state = local_rand_state;
        *d_world  = new hitable_list(d_list, num_hitables);
    }
}

__global__ void free_world(hitable **d_list, hitable **d_world, camera **d_camera, int num_hitables) {
    for(int i=0; i < num_hitables; i++) {
        delete ((sphere *)d_list[i])->mat_ptr;
        delete d_list[i];
    }
}


void intialize_variables(int nx, int ny, bool isSmall) {
    /**
     * @param isSmall: flags the need for creating "small" world
     */

    CACHED_NX = nx;
    CACHED_NY = ny;

    blocks = dim3(nx / tx + 1, ny / ty + 1);
    threads = dim3(tx, ty);
    int num_pixels = nx * ny;
    fb_size = num_pixels * sizeof(vec3);

    checkCudaErrors(cudaMallocManaged((void **)&fb, fb_size));
    checkCudaErrors(cudaMalloc((void **)&d_rand_state, num_pixels * sizeof(curandState)));
    checkCudaErrors(cudaMalloc((void **)&d_rand_state_world, 1 * sizeof(curandState)));

    rand_init<<<1, 1>>>(d_rand_state_world);
    render_init<<<blocks, threads>>>(nx, ny, d_rand_state);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());

    if (isSmall) {
        CACHED_NUM_SPHERES = 5;
        checkCudaErrors(cudaMalloc((void **)&d_list, CACHED_NUM_SPHERES * sizeof(hitable *)));
        checkCudaErrors(cudaMalloc((void **)&d_world, sizeof(hitable *)));
        craete_small_world<<<1, 1>>>(d_list, d_world, d_rand_state_world);
    } else {
        CACHED_NUM_SPHERES = 22 * 22 + 1 + 3;
        checkCudaErrors(cudaMalloc((void **)&d_list, CACHED_NUM_SPHERES * sizeof(hitable *)));
        checkCudaErrors(cudaMalloc((void **)&d_world, sizeof(hitable *)));
        create_world<<<1, 1>>>(d_list, d_world, d_rand_state_world, CACHED_NUM_SPHERES);
    }

    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
}


void set_camera_position(int nx, int ny, float fov, bool isSmall) {
    CACHED_FOV = fov;
    vec3 lookfrom, lookat;
    if (isSmall) {
        lookfrom = vec3(-2, 2, 1);
        lookat = vec3(0, 0, -1);
    } else {
        lookfrom = vec3(13, 2, 3);
        lookat = vec3(0, 0, 0);
    }

    checkCudaErrors(cudaMalloc((void **)&d_camera, sizeof(camera *)));
    set_camera<<<1, 1>>>(d_camera, lookfrom, lookat, fov, nx, ny);
    checkCudaErrors(cudaDeviceSynchronize());
}


float launch_render(int *image, int nx, int ny, int ns) {
    /**
     * @param nx: image width
     * @param ny: image height
     * @param ns: "number of samples" 
     * 
     * @details 
     * Here using not a best method for time consumption measuring
     * btw i don't think it is important in such case
     */
    clock_t start, stop;
    start = clock();
    render<<<blocks, threads>>>(fb, nx, ny, ns, d_camera, d_world, d_rand_state);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
    stop = clock();
    double timer_seconds = ((double)(stop - start)) / CLOCKS_PER_SEC;

    // Move buffer to our existing 1D (3D) array
    for (int j = ny-1; j >= 0; j--) {
        for (int i = 0; i < nx; i++) {
            size_t pixel_index = j * nx + i;
            size_t image_index = (j * nx + i) * 3;
            image[image_index + 0] = int(255.99*fb[pixel_index].r());
            image[image_index + 1] = int(255.99*fb[pixel_index].g());
            image[image_index + 2] = int(255.99*fb[pixel_index].b());
        }
    }

    return timer_seconds;
}


void clear_cuda() {
    free_world<<<1, 1>>>(d_list, d_world, d_camera, CACHED_NUM_SPHERES);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaFree(d_world));
    checkCudaErrors(cudaFree(d_list));
    checkCudaErrors(cudaFree(d_rand_state));
    checkCudaErrors(cudaFree(d_rand_state_world));
    checkCudaErrors(cudaFree(fb));
}


float small_launch_render(int *image, int nx, int ny, int ns, float fov) {
    /**
     * @brief launch render with world which contains only 5 spheres
     * @details Useful to accomplish test CPU vs GPU
     */
    if (CACHED_NX != nx) {
        if (CACHED_NX != NULL) {
            clear_cuda();
        }
        intialize_variables(nx, ny, true);
    }
    if (CACHED_FOV != fov) {
        if (CACHED_FOV != NULL) {
            checkCudaErrors(cudaFree(d_camera));
        }
        set_camera_position(nx, ny, fov, true);
    }
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
    return launch_render(image, nx, ny, ns);
}


float big_launch_render(int *image, int nx, int ny, int ns, float fov) {
    /**
     * @brief launch render with "big" world which contains about 22^2 spheres 
     * @details Useful for getting info about frametime 
     */
    if (CACHED_NX != nx) {
        if (CACHED_NX != NULL) {
            clear_cuda();
        }
        intialize_variables(nx, ny, false);
    }
    if (CACHED_FOV != fov) {
        if (CACHED_FOV != NULL) {
            checkCudaErrors(cudaFree(d_camera));
        }
        set_camera_position(nx, ny, fov, false);
    }
    return launch_render(image, nx, ny, ns);
}