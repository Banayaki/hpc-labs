#ifndef RAYH
#define RAYH
#include "vec3.h"

class ray {
public: 
    __device__ ray() {}
    __device__ ray(const vec3& a, const vec3& b) : A(a), B(b) {};
    __device__ vec3 origin() const;
    __device__ vec3 direction() const;
    __device__ vec3 point_at(float t) const;

    vec3 A;
    vec3 B;
};

#endif