#include "include/ray.h"

__device__ vec3 ray::origin() const {
    return A;
}

__device__ vec3 ray::direction() const {
    return B;
}

__device__ vec3 ray::point_at(float t) const {
    return A + t * B;
}
